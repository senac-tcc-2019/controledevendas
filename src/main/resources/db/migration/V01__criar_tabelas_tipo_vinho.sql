CREATE TABLE tipo (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE vinho (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    sku VARCHAR(50) NOT NULL,
    nome VARCHAR(80) NOT NULL,
    descricao TEXT NOT NULL,
    valor DECIMAL(10, 2) NOT NULL,
    quantidade_estoque INTEGER,
    teor_alcoolico DECIMAL(10, 2) NOT NULL,
    comissao DECIMAL(10, 2) NOT NULL,
    uva VARCHAR(50) NOT NULL,
    origem VARCHAR(50) NOT NULL,
    codigo_tipo BIGINT(20) NOT NULL,
    FOREIGN KEY (codigo_tipo) REFERENCES tipo(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO tipo VALUES (0, 'Tinto');
INSERT INTO tipo VALUES (0, 'Branco');
INSERT INTO tipo VALUES (0, 'Rosé');
INSERT INTO tipo VALUES (0, 'Espumante');