INSERT INTO permissao VALUES (3, 'ROLE_CADASTRAR_ESTADO');
INSERT INTO permissao VALUES (4, 'ROLE_CADASTRAR_VINHO');
INSERT INTO permissao VALUES (5, 'ROLE_CADASTRAR_TIPO');

INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (1, 3);
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (1, 4);
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (1, 5);