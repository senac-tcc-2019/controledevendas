package com.senac.stock.dto;

import com.senac.stock.model.Origem;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class VinhoDTO {

    private Long codigo;
    private String sku;
    private String nome;
    private String origem;
    private BigDecimal valor;
    private String foto;

    public VinhoDTO(Long codigo, String sku, String nome, Origem origem, BigDecimal valor, String foto) {
        this.codigo = codigo;
        this.sku = sku;
        this.nome = nome;
        this.origem = origem.getDescricao();
        this.valor = valor;
        this.foto = foto;
    }
}
