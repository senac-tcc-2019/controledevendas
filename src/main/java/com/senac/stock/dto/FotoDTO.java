package com.senac.stock.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FotoDTO {

    private String nome;
    private String contentType;

    public FotoDTO(String nome, String contentType) {
        this.nome = nome;
        this.contentType = contentType;
    }
}
