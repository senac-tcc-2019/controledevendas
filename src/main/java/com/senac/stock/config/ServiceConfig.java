package com.senac.stock.config;

import com.senac.stock.service.CadastroVinhoService;
import com.senac.stock.storage.FotoStorage;
import com.senac.stock.storage.local.FotoStorageLocal;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = CadastroVinhoService.class)
public class ServiceConfig {

    @Bean
    public FotoStorage fotoStorage(){
        return new FotoStorageLocal();
    }
}
