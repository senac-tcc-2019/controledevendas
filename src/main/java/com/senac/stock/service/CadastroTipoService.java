package com.senac.stock.service;

import com.senac.stock.model.Tipo;
import com.senac.stock.repository.TipoRepository;
import com.senac.stock.service.exception.NomeTipoJaExistenteException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class CadastroTipoService {

    @Autowired
    private TipoRepository tipoRepository;

    public void salvar(Tipo tipo){
        Optional<Tipo> tipoOptional = tipoRepository.findByNomeIgnoreCase(tipo.getNome());

        if(tipoOptional.isPresent()){
            throw new NomeTipoJaExistenteException("Nome já está cadastrado");
        }

        tipoRepository.save(tipo);
    }

    @Transactional
    public void excluir(Long codigo){
        tipoRepository.deleteById(codigo);
    }
}
