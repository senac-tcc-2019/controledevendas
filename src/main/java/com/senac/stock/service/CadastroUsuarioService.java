package com.senac.stock.service;

import com.senac.stock.model.Usuario;
import com.senac.stock.repository.UsuarioRepository;
import com.senac.stock.service.exception.EmailUsuarioJaCadastradoException;
import com.senac.stock.service.exception.SenhaObrigatoriaUsuarioException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Service
public class CadastroUsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    public void salvar(Usuario usuario) {
        Optional<Usuario> usuarioExistente = usuarioRepository.findByEmail(usuario.getEmail());
        if (usuarioExistente.isPresent() && !usuarioExistente.get().equals(usuario)) {
            throw new EmailUsuarioJaCadastradoException("E-mail já cadastrado");
        }

        if (usuario.isNovo() && StringUtils.isEmpty(usuario.getSenha())) {
            throw new SenhaObrigatoriaUsuarioException("Senha é obrigatória para novo usuário");
        }

        if(!usuario.getSenha().equals(usuario.getConfirmacaoSenha())){
            throw new SenhaObrigatoriaUsuarioException("Senhas não conferem");
        }


        if (usuario.isNovo() || !StringUtils.isEmpty(usuario.getSenha())) {
            usuario.setSenha(this.passwordEncoder.encode(usuario.getSenha()));
        } else if (StringUtils.isEmpty(usuario.getSenha())) {
            usuario.setSenha(usuarioExistente.get().getSenha());

        }
        usuario.setConfirmacaoSenha(usuario.getSenha());

        if (!usuario.isNovo() && usuario.getAtivo() == null) {
            usuario.setAtivo(usuarioExistente.get().getAtivo());
        }
        System.out.println(usuario.getSenha().equals(usuario.getConfirmacaoSenha()));
        usuarioRepository.save(usuario);
    }
}
