package com.senac.stock.service;

import com.senac.stock.model.Estado;
import com.senac.stock.repository.EstadoRepository;
import com.senac.stock.service.exception.ImpossivelExcluirException;
import com.senac.stock.service.exception.NomeTipoJaExistenteException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import java.util.Optional;

@Service
public class CadastroEstadoService {

    @Autowired
    private EstadoRepository estadoRepository;

    public void salvar(Estado estado){
        Optional<Estado> tipoOptional = estadoRepository.findByNomeIgnoreCase(estado.getNome());

        if(tipoOptional.isPresent()){
            throw new NomeTipoJaExistenteException("Nome já está cadastrado");
        }

        estadoRepository.save(estado);
    }

    @Transactional
    public void excluir(Long codigo){
        try {
            estadoRepository.deleteById(codigo);
            estadoRepository.flush();
        } catch (ConstraintViolationException e){
            throw new ImpossivelExcluirException("Impossivel apagar o estado. Já está sendo utilizado, apague as cidades antes!");
        }
    }
}
