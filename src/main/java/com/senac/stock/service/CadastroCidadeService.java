package com.senac.stock.service;

import com.senac.stock.model.Cidade;
import com.senac.stock.repository.CidadeRepository;
import com.senac.stock.service.exception.NomeTipoJaExistenteException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class CadastroCidadeService {

    @Autowired
    private CidadeRepository cidadeRepository;

    public void salvar(Cidade cidade){
        Optional<Cidade> cidadeOptional = cidadeRepository.findByNomeAndEstado(cidade.getNome(), cidade.getEstado());

        if(cidadeOptional.isPresent()){
            throw new NomeTipoJaExistenteException("Nome já está cadastrado");
        }
        cidadeRepository.save(cidade);
    }

    @Transactional
    public void excluir(Long codigo){
        cidadeRepository.deleteById(codigo);
    }
}
