package com.senac.stock.service.exception;

public class CpfOuCnpjClienteJaCadastradoException extends RuntimeException {

    public CpfOuCnpjClienteJaCadastradoException(String message) {
        super(message);
    }
}
