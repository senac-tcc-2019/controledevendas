package com.senac.stock.service.exception;

public class SenhaObrigatoriaUsuarioException extends RuntimeException {

    public SenhaObrigatoriaUsuarioException(String message){
        super(message);
    }

}
