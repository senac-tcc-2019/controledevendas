package com.senac.stock.service.exception;

public class EmailUsuarioJaCadastradoException extends RuntimeException{

    public EmailUsuarioJaCadastradoException(String message) {
        super(message);
    }
}
