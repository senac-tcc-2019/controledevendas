package com.senac.stock.service.exception;

public class NomeTipoJaExistenteException extends RuntimeException {
    public NomeTipoJaExistenteException(String message) {
        super(message);
    }
}
