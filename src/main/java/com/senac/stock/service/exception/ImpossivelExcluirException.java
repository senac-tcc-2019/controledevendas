package com.senac.stock.service.exception;

public class ImpossivelExcluirException extends RuntimeException {

    public ImpossivelExcluirException(String msg) {
        super(msg);
    }
}
