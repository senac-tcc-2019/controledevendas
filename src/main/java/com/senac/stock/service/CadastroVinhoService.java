package com.senac.stock.service;

import com.senac.stock.model.Vinho;
import com.senac.stock.repository.VinhoRepository;
import com.senac.stock.service.event.vinho.VinhoSalvaEvent;
import com.senac.stock.service.exception.ImpossivelExcluirException;
import com.senac.stock.storage.FotoStorage;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;

@Service
public class CadastroVinhoService {

    @Autowired
    private VinhoRepository vinhoRepository;

    @Autowired
    private FotoStorage fotoStorage;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Transactional
    public void salvar(Vinho vinho){
        vinhoRepository.save(vinho);

        applicationEventPublisher.publishEvent(new VinhoSalvaEvent(vinho));
    }

    @Transactional
    public void excluir(Vinho vinho){
        try {
            String foto = vinho.getFoto();
            vinhoRepository.delete(vinho);
            vinhoRepository.flush();
            fotoStorage.excluir(foto);
        } catch (ConstraintViolationException e){
            throw new ImpossivelExcluirException("Impossivel excluir o vinho. Já está sendo utilizado!");
        }
    }
}
