package com.senac.stock.service;

import com.senac.stock.model.Cliente;
import com.senac.stock.repository.ClienteRepository;
import com.senac.stock.service.exception.CpfOuCnpjClienteJaCadastradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class CadastroClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Transactional
    public void salvar(Cliente cliente){

        Optional<Cliente> clienteExistente = clienteRepository.findByCpfOuCnpj(cliente.getCpfOuCnpjSemFormatacao());
        if(clienteExistente.isPresent() && !clienteExistente.get().equals(cliente)){
            throw new CpfOuCnpjClienteJaCadastradoException("CPF/CNPJ já está cadastrado no sistema");
        }

        clienteRepository.save(cliente);
    }

    @Transactional
    public void excluir(Long codigo){
        clienteRepository.deleteById(codigo);
    }

}
