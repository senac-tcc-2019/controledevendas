package com.senac.stock.service.event.vinho;

import com.senac.stock.storage.FotoStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class VinhoListener {

    @Autowired
    private FotoStorage fotoStorage;

    @EventListener(condition = "#event.temFoto()")
    public void vinhoSalva(VinhoSalvaEvent event){
        fotoStorage.salvar(event.getVinho().getFoto());
    }
}
