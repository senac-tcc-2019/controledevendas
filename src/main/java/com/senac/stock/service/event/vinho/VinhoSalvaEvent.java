package com.senac.stock.service.event.vinho;

import com.senac.stock.model.Vinho;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Getter
@Setter
public class VinhoSalvaEvent {

    private Vinho vinho;

    public VinhoSalvaEvent(Vinho vinho) {
        this.vinho = vinho;
    }

    public boolean temFoto(){
        return !StringUtils.isEmpty(vinho.getFoto());
    }
}
