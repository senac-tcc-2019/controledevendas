package com.senac.stock.repository;

import com.senac.stock.model.Tipo;
import com.senac.stock.repository.helper.tipo.TipoRepositoryQueries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TipoRepository extends JpaRepository<Tipo, Long>, TipoRepositoryQueries {

    public Optional<Tipo> findByNomeIgnoreCase(String nome);
}
