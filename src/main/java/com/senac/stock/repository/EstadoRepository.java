package com.senac.stock.repository;

import com.senac.stock.model.Estado;
import com.senac.stock.repository.helper.estado.EstadoRepositoryQueries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EstadoRepository extends JpaRepository<Estado, Long>, EstadoRepositoryQueries {

    public Optional<Estado> findByNomeIgnoreCase(String nome);
}
