package com.senac.stock.repository;

import com.senac.stock.model.Cliente;
import com.senac.stock.repository.helper.cliente.ClienteRepositoryQueries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long>, ClienteRepositoryQueries {

    public Optional<Cliente> findByCpfOuCnpj(String cpfOuCnpj);

    List<Cliente> findByNomeStartingWithIgnoreCase(String nome);
}
