package com.senac.stock.repository.filter;

import com.senac.stock.model.Origem;
import com.senac.stock.model.Tipo;
import com.senac.stock.model.Uva;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class VinhoFilter {

    private String sku;
    private String nome;
    private Tipo tipo;
    private Uva uva;
    private Origem origem;
    private BigDecimal valorDe;
    private BigDecimal valorAte;

}
