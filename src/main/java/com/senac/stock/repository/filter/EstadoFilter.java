package com.senac.stock.repository.filter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EstadoFilter {

    private String nome;
}
