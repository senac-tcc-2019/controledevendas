package com.senac.stock.repository.filter;

import com.senac.stock.model.Estado;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CidadeFilter {

    private String nome;
    private Estado estado;
}
