package com.senac.stock.repository;

import com.senac.stock.model.Vinho;
import com.senac.stock.repository.helper.vinho.VinhoRepositoryQueries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface VinhoRepository extends JpaRepository<Vinho, Long>, VinhoRepositoryQueries {

}
