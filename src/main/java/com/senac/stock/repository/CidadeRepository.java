package com.senac.stock.repository;

import com.senac.stock.model.Cidade;
import com.senac.stock.model.Estado;
import com.senac.stock.repository.helper.cidade.CidadeRepositoryQueries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CidadeRepository extends JpaRepository<Cidade, Long>, CidadeRepositoryQueries {

    public Optional<Cidade> findByNomeIgnoreCase(String nome);

    public List<Cidade> findByEstadoCodigo(Long codigoEstado);

    public Optional<Cidade> findByNomeAndEstado(String nome, Estado estado);
}
