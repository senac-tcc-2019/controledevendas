package com.senac.stock.repository.helper.estado;

import com.senac.stock.model.Estado;
import com.senac.stock.repository.filter.EstadoFilter;

import java.util.List;

public interface EstadoRepositoryQueries {

    public List<Estado> filtrar(EstadoFilter filtro);
}
