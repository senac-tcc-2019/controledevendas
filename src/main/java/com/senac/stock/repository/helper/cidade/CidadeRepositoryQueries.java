package com.senac.stock.repository.helper.cidade;

import com.senac.stock.model.Cidade;
import com.senac.stock.repository.filter.CidadeFilter;

import java.util.List;

public interface CidadeRepositoryQueries {

    public List<Cidade> filtrar(CidadeFilter filtro);
}
