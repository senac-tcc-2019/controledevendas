package com.senac.stock.repository.helper.cliente;

import com.senac.stock.model.Cliente;
import com.senac.stock.repository.filter.ClienteFilter;

import java.util.List;

public interface ClienteRepositoryQueries {

    public List<Cliente> filtrar(ClienteFilter filtro);
}
