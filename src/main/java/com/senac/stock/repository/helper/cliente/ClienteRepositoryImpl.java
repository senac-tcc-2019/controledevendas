package com.senac.stock.repository.helper.cliente;

import com.senac.stock.model.Cliente;
import com.senac.stock.repository.filter.ClienteFilter;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class ClienteRepositoryImpl implements ClienteRepositoryQueries{

    @PersistenceContext
    private EntityManager manager;

    @SuppressWarnings("unchecked")
    @Override
    @Transactional(readOnly = true)
    public List<Cliente> filtrar(ClienteFilter filtro) {
        Criteria criteria = manager.unwrap(Session.class).createCriteria(Cliente.class);

        criteria.createAlias("endereco.cidade", "c", JoinType.LEFT_OUTER_JOIN);

        criteria.createAlias("c.estado", "e", JoinType.LEFT_OUTER_JOIN);

        if(filtro != null) {
            if (!StringUtils.isEmpty(filtro.getNome())) {
                criteria.add(Restrictions.ilike("nome", filtro.getNome(), MatchMode.ANYWHERE));
            }

            if (!StringUtils.isEmpty(filtro.getCpfOuCnpj())) {
                criteria.add(Restrictions.eq("cpfOuCnpj", filtro.getCpfOuCnpjSemFormatacao()));
            }
        }

        return criteria.list();
    }
}
