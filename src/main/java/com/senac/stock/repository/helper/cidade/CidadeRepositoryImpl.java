package com.senac.stock.repository.helper.cidade;

import com.senac.stock.model.Cidade;
import com.senac.stock.repository.filter.CidadeFilter;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class CidadeRepositoryImpl implements CidadeRepositoryQueries {

    @PersistenceContext
    private EntityManager manager;

    @SuppressWarnings("unchecked")
    @Override
    @Transactional(readOnly = true)
    public List<Cidade> filtrar(CidadeFilter filtro) {
        Criteria criteria = manager.unwrap(Session.class).createCriteria(Cidade.class);

        criteria.createAlias("estado", "e", JoinType.LEFT_OUTER_JOIN);

        if(filtro != null) {

            if (filtro.getEstado() != null) {
                criteria.add(Restrictions.eq("estado", filtro.getEstado()));
            }

            if (!StringUtils.isEmpty(filtro.getNome())) {
                criteria.add(Restrictions.ilike("nome", filtro.getNome(), MatchMode.ANYWHERE));
            }
        }

        return criteria.list();
    }
}
