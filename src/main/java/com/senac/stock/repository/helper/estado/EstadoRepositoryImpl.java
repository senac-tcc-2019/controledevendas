package com.senac.stock.repository.helper.estado;

import com.senac.stock.model.Estado;
import com.senac.stock.repository.filter.EstadoFilter;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class EstadoRepositoryImpl implements EstadoRepositoryQueries{

    @PersistenceContext
    private EntityManager manager;

    @SuppressWarnings("unchecked")
    @Override
    @Transactional(readOnly = true)
    public List<Estado> filtrar(EstadoFilter filtro) {
        Criteria criteria = manager.unwrap(Session.class).createCriteria(Estado.class);

        if(filtro != null) {
            if (!StringUtils.isEmpty(filtro.getNome())) {
                criteria.add(Restrictions.ilike("nome", filtro.getNome(), MatchMode.ANYWHERE));
            }
        }

        return criteria.list();
    }
}
