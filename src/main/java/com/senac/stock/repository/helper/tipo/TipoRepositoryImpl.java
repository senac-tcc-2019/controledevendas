package com.senac.stock.repository.helper.tipo;

import com.senac.stock.model.Tipo;
import com.senac.stock.repository.filter.TipoFilter;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class TipoRepositoryImpl implements TipoRepositoryQueries {


    @PersistenceContext
    private EntityManager manager;

    @SuppressWarnings("unchecked")
    @Override
    @Transactional(readOnly = true)
    public List<Tipo> filtrar(TipoFilter filtro) {
        Criteria criteria = manager.unwrap(Session.class).createCriteria(Tipo.class);

        if(filtro != null) {
            if (!StringUtils.isEmpty(filtro.getNome())) {
                criteria.add(Restrictions.ilike("nome", filtro.getNome(), MatchMode.ANYWHERE));
            }
        }

        return criteria.list();
    }
}
