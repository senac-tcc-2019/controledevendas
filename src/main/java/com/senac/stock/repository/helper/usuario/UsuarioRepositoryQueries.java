package com.senac.stock.repository.helper.usuario;

import com.senac.stock.model.Usuario;
import com.senac.stock.repository.filter.UsuarioFilter;

import java.util.List;
import java.util.Optional;

public interface UsuarioRepositoryQueries {

    public Optional<Usuario> porEmailEAtivo(String email);

    public List<String> permissoes(Usuario usuario);

    public List<Usuario> filtrar(UsuarioFilter filtro);
}
