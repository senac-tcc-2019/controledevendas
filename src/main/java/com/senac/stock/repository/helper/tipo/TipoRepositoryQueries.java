package com.senac.stock.repository.helper.tipo;

import com.senac.stock.model.Tipo;
import com.senac.stock.repository.filter.TipoFilter;

import java.util.List;

public interface TipoRepositoryQueries {

    public List<Tipo> filtrar(TipoFilter filtro);
}
