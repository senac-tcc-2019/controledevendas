package com.senac.stock.repository.helper.vinho;

import com.senac.stock.dto.VinhoDTO;
import com.senac.stock.model.Vinho;
import com.senac.stock.repository.filter.VinhoFilter;

import java.util.List;

public interface VinhoRepositoryQueries {

    public List<Vinho> filtrar(VinhoFilter filtro);

    public List<VinhoDTO> porSkuOuNome(String porSkuOuNome);
}
