package com.senac.stock.repository.helper.vinho;

import com.senac.stock.dto.VinhoDTO;
import com.senac.stock.model.Vinho;
import com.senac.stock.repository.filter.VinhoFilter;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class VinhoRepositoryImpl implements VinhoRepositoryQueries {


    @PersistenceContext
    private EntityManager manager;


    @SuppressWarnings("unchecked")
    @Override
    @Transactional(readOnly = true)
    public List<Vinho> filtrar(VinhoFilter filtro) {
        Criteria criteria = manager.unwrap(Session.class).createCriteria(Vinho.class);

        if(filtro != null){
            if(!StringUtils.isEmpty(filtro.getSku())){
                criteria.add(Restrictions.eq("sku", filtro.getSku()));
            }

            if(!StringUtils.isEmpty(filtro.getNome())){
                criteria.add(Restrictions.ilike("nome", filtro.getNome(), MatchMode.ANYWHERE));
            }

            if (isTipoPresente(filtro)) {
                criteria.add(Restrictions.eq("tipo", filtro.getTipo()));
            }

            if (filtro.getUva() != null) {
                criteria.add(Restrictions.eq("uva", filtro.getUva()));
            }

            if (filtro.getOrigem() != null) {
                criteria.add(Restrictions.eq("origem", filtro.getOrigem()));
            }

            if (filtro.getValorDe() != null) {
                criteria.add(Restrictions.ge("valor", filtro.getValorDe()));
            }

            if (filtro.getValorAte() != null) {
                criteria.add(Restrictions.le("valor", filtro.getValorAte()));
            }
        }

        return criteria.list();
    }

    @Override
    public List<VinhoDTO> porSkuOuNome(String skuOuNome) {
        String jpql = "select new com.senac.stock.dto.VinhoDTO(codigo, sku, nome, origem, valor, foto) " +
                "from Vinho where lower(sku) like :skuOuNome or lower(nome) like :skuOuNome";
        List<VinhoDTO> vinhosFiltrados = manager.createQuery(jpql, VinhoDTO.class).setParameter("skuOuNome", skuOuNome.toLowerCase() + "%").getResultList();
        return vinhosFiltrados;
    }

    private boolean isTipoPresente(VinhoFilter filtro) {
        return filtro.getTipo() != null && filtro.getTipo().getCodigo() != null;
    }
}
