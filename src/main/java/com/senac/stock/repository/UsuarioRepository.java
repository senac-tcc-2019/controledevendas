package com.senac.stock.repository;

import com.senac.stock.model.Usuario;
import com.senac.stock.repository.helper.usuario.UsuarioRepositoryQueries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>, UsuarioRepositoryQueries {

    public Optional<Usuario> findByEmail(String email);
}
