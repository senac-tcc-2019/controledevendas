package com.senac.stock.controller;

import com.senac.stock.dto.VinhoDTO;
import com.senac.stock.model.Origem;
import com.senac.stock.model.Uva;
import com.senac.stock.model.Vinho;
import com.senac.stock.repository.TipoRepository;
import com.senac.stock.repository.VinhoRepository;
import com.senac.stock.repository.filter.VinhoFilter;
import com.senac.stock.service.CadastroVinhoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/vinhos")
public class VinhosController {

    @Autowired
    private CadastroVinhoService cadastroVinhoService;

    @Autowired
    private VinhoRepository vinhoRepository;

    @Autowired
    private TipoRepository tipoRepository;

    @GetMapping("/novo")
    public ModelAndView novo(Vinho vinho) {
        ModelAndView modelAndView = new ModelAndView("vinho/CadastroVinho");
        modelAndView.addObject("uvas", Uva.values());
        modelAndView.addObject("tipos", tipoRepository.findAll());
        modelAndView.addObject("origens", Origem.values());
        return modelAndView;
    }

    @PostMapping({"/novo", "{\\d+}"})
    public ModelAndView cadastrar(@Valid Vinho vinho, BindingResult result, Model model, RedirectAttributes attributes) {
       if (result.hasErrors()) {
           return novo(vinho);
       }
        cadastroVinhoService.salvar(vinho);

        attributes.addFlashAttribute("mensagem", "Vinho salvo com sucesso!");

        return new ModelAndView("redirect:/vinhos");
    }

    @GetMapping()
    public ModelAndView pesquisar(VinhoFilter vinhoFilter, BindingResult result, Pageable pageable){
        ModelAndView modelAndView = new ModelAndView("vinho/PesquisaVinhos");
        modelAndView.addObject("tipos",tipoRepository.findAll());
        modelAndView.addObject("uvas", Uva.values());
        modelAndView.addObject("origens", Origem.values());

        modelAndView.addObject("vinhos", vinhoRepository.filtrar(vinhoFilter));
        return modelAndView;
    }

    @DeleteMapping("{codigo}")
    public @ResponseBody ResponseEntity<?> excluir(@PathVariable("codigo") Vinho vinho){
        try {
            cadastroVinhoService.excluir(vinho);
        } catch (ConstraintViolationException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{codigo}")
    public ModelAndView editar(@PathVariable("codigo") Vinho vinho){
        ModelAndView modelAndView = novo(vinho);
        modelAndView.addObject(vinho);
        return modelAndView;
    }

    @GetMapping("filtro")
    public @ResponseBody List<VinhoDTO> pesquisar(String skuOuNome){
        return vinhoRepository.porSkuOuNome(skuOuNome);
    }
}
