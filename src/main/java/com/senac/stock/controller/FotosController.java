package com.senac.stock.controller;

import com.senac.stock.dto.FotoDTO;
import com.senac.stock.storage.FotoStorage;
import com.senac.stock.storage.FotoStorageRunnable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;

/* RestController para não precisar colocar @ResponseBody
no metodo, pois o RestController já implementa ele*/
@RestController
@RequestMapping("stock/fotos")
public class FotosController {

    @Autowired
    private FotoStorage fotoStorage;

    @PostMapping
    public DeferredResult<FotoDTO> upload(@RequestParam("files[]") MultipartFile[] files){
        DeferredResult<FotoDTO> resultado = new DeferredResult<>();

        Thread thread = new Thread(new FotoStorageRunnable(files, resultado, fotoStorage));
        thread.start();

        return resultado;
    }

    @GetMapping("/temp/{nome:.*}")
    public byte[] recuperarFotoTemp(@PathVariable String nome){
        return fotoStorage.recuperarFotoTemp(nome);
    }

    @GetMapping("/{nome:.*}")
    public byte[] recuperar(@PathVariable String nome){
        return fotoStorage.recuperar(nome);
    }
}
