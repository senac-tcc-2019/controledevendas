package com.senac.stock.controller;

import com.senac.stock.model.Estado;
import com.senac.stock.repository.EstadoRepository;
import com.senac.stock.repository.filter.EstadoFilter;
import com.senac.stock.service.CadastroEstadoService;
import com.senac.stock.service.exception.ImpossivelExcluirException;
import com.senac.stock.service.exception.NomeTipoJaExistenteException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

@Controller
@RequestMapping("/estados")
public class EstadosController {

    @Autowired
    private CadastroEstadoService cadastroEstadoService;

    @Autowired
    private EstadoRepository estadoRepository;

    @RequestMapping("/novo")
    public ModelAndView novo(Estado estado) {
        return new ModelAndView("estado/CadastroEstado");
    }

    @PostMapping({"/novo", "{\\d+}"})
    public ModelAndView cadastrar(@Valid Estado estado, BindingResult bindingResult, RedirectAttributes redirectAttributes){
        if (bindingResult.hasErrors()) {
            return novo(estado);
        }

        try {
            cadastroEstadoService.salvar(estado);
        }catch (NomeTipoJaExistenteException e){
            bindingResult.reject("nome", e.getMessage());
            return novo(estado);
        }
        redirectAttributes.addFlashAttribute("mensagem", "Estado salvo com sucesso");
        return new ModelAndView("redirect:/estados");

    }

    @GetMapping()
    public ModelAndView pesquisar(EstadoFilter estadoFilter, BindingResult result, Pageable pageable){
        ModelAndView modelAndView = new ModelAndView("estado/PesquisaEstado");

        modelAndView.addObject("estados", estadoRepository.filtrar(estadoFilter));
        return modelAndView;
    }

    @DeleteMapping("/{codigo}")
    public @ResponseBody ResponseEntity<?> excluir(@PathVariable("codigo") Long codigo){

        try {
            cadastroEstadoService.excluir(codigo);
        } catch (ConstraintViolationException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{codigo}")
    public ModelAndView editar(@PathVariable("codigo") Estado estado){
        ModelAndView modelAndView = novo(estado);
        modelAndView.addObject(estado);
        return modelAndView;
    }

}
