package com.senac.stock.controller;

import com.senac.stock.model.Usuario;
import com.senac.stock.repository.GrupoRepository;
import com.senac.stock.repository.UsuarioRepository;
import com.senac.stock.repository.filter.ClienteFilter;
import com.senac.stock.repository.filter.UsuarioFilter;
import com.senac.stock.service.CadastroUsuarioService;
import com.senac.stock.service.exception.EmailUsuarioJaCadastradoException;
import com.senac.stock.service.exception.SenhaObrigatoriaUsuarioException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/usuarios")
public class UsuariosController {

    @Autowired
    private CadastroUsuarioService cadastroUsuarioService;

    @Autowired
    private GrupoRepository grupoRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @GetMapping("/novo")
    public ModelAndView novo(Usuario usuario) {
        ModelAndView modelAndView = new ModelAndView("usuario/CadastroUsuario");
        modelAndView.addObject("grupos", grupoRepository.findAll());
        return modelAndView;
    }

    @PostMapping({"/novo", "{\\+d}"})
    public ModelAndView salvar(@Valid Usuario usuario, BindingResult result, RedirectAttributes attributes) {
        if (result.hasErrors()) {
            return novo(usuario);
        }

        try {
            cadastroUsuarioService.salvar(usuario);
        } catch (EmailUsuarioJaCadastradoException e) {
            result.rejectValue("email", e.getMessage(), e.getMessage());
            return novo(usuario);
        } catch (SenhaObrigatoriaUsuarioException e) {
            result.rejectValue("senha", e.getMessage(), e.getMessage());
            return novo(usuario);
        }

        attributes.addFlashAttribute("mensagem", "Usuário salvo com sucesso");
        return new ModelAndView("redirect:/usuarios");
    }

    @InitBinder
    public void initBinder(WebDataBinder binder){
        binder.registerCustomEditor(       Date.class,
                new CustomDateEditor(new SimpleDateFormat("dd/MM/yyyy"), true, 10));
    }

    @GetMapping
    public ModelAndView pesquisar(UsuarioFilter usuarioFilter) {
        ModelAndView mv = new ModelAndView("usuario/PesquisaUsuarios");
        mv.addObject("usuarios", usuarioRepository.filtrar(usuarioFilter));
        mv.addObject("grupos", grupoRepository.findAll());
        return mv;
    }

    @GetMapping("/{codigo}")
    public ModelAndView editar(@PathVariable("codigo") Usuario usuario){
        ModelAndView mv = novo(usuario);
        mv.addObject(usuario);
        return mv;
    }
}
