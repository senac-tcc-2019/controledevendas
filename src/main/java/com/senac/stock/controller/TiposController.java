package com.senac.stock.controller;

import com.senac.stock.model.Tipo;
import com.senac.stock.repository.TipoRepository;
import com.senac.stock.repository.filter.TipoFilter;
import com.senac.stock.service.CadastroTipoService;
import com.senac.stock.service.exception.NomeTipoJaExistenteException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/tipos")
public class TiposController {

    @Autowired
    private CadastroTipoService cadastroTipoService;

    @Autowired
    private TipoRepository tipoRepository;

    @RequestMapping("/novo")
    public ModelAndView novo(Tipo tipo) {
        return new ModelAndView("tipo/CadastroTipo");
    }

    @PostMapping({"/novo", "{\\d+}"})
    public ModelAndView cadastrar(@Valid Tipo tipo, BindingResult bindingResult, RedirectAttributes redirectAttributes){
        if (bindingResult.hasErrors()) {
            return novo(tipo);
        }

        try {
        cadastroTipoService.salvar(tipo);
        }catch (NomeTipoJaExistenteException e){
            bindingResult.reject("nome", e.getMessage());
            return novo(tipo);
        }
        redirectAttributes.addFlashAttribute("mensagem", "Tipo salvo com sucesso");
        return new ModelAndView("redirect:/tipos");

    }

    @GetMapping()
    public ModelAndView pesquisar(TipoFilter tipoFilter, BindingResult result, Pageable pageable){
        ModelAndView modelAndView = new ModelAndView("tipo/PesquisaTipo");

        modelAndView.addObject("tipos", tipoRepository.filtrar(tipoFilter));
        return modelAndView;
    }

    @DeleteMapping("{codigo}")
    public @ResponseBody
    ResponseEntity<?> excluir(@PathVariable("codigo") Long codigo){
        cadastroTipoService.excluir(codigo);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/{codigo}")
    public ModelAndView editar(@PathVariable("codigo") Tipo tipo){
        ModelAndView modelAndView = novo(tipo);
        modelAndView.addObject(tipo);
        return modelAndView;
    }
}
