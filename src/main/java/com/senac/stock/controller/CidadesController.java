package com.senac.stock.controller;

import com.senac.stock.model.Cidade;
import com.senac.stock.repository.CidadeRepository;
import com.senac.stock.repository.EstadoRepository;
import com.senac.stock.repository.filter.CidadeFilter;
import com.senac.stock.service.CadastroCidadeService;
import com.senac.stock.service.exception.NomeTipoJaExistenteException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/cidades")
public class CidadesController {

    @Autowired
    private CadastroCidadeService cadastroCidadeService;

    @Autowired
    private CidadeRepository cidadeRepository;

    @Autowired
    private EstadoRepository estadoRepository;

    @GetMapping("/novo")
    public ModelAndView novo(Cidade cidade) {
        ModelAndView modelAndView = new ModelAndView("cidade/CadastroCidade");
        modelAndView.addObject("estados", estadoRepository.findAll());
        return modelAndView;
    }

    @PostMapping({"/novo", "{\\d+}"})
    public ModelAndView cadastrar(@Valid Cidade cidade, BindingResult bindingResult, Model model, RedirectAttributes attributes) {
        if (bindingResult.hasErrors()) {
            return novo(cidade);
        }
        try {
            cadastroCidadeService.salvar(cidade);
        }catch (NomeTipoJaExistenteException e){
            bindingResult.reject("nome", e.getMessage());
            return novo(cidade);
        }
        attributes.addFlashAttribute("mensagem", "Cidade salvo com sucesso!");

        return new ModelAndView("redirect:/cidades");
    }

    @GetMapping()
    public ModelAndView pesquisar(CidadeFilter cidadeFilter, BindingResult result, Pageable pageable){
        ModelAndView modelAndView = new ModelAndView("cidade/PesquisaCidade");
        modelAndView.addObject("estados", estadoRepository.findAll());

        modelAndView.addObject("cidades", cidadeRepository.filtrar(cidadeFilter));
        return modelAndView;
    }

    @RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<Cidade> pesqusiarPorCodigoEstado(@RequestParam(name = "estado", defaultValue = "-1") Long codigoEstado){
        try {
            Thread.sleep(500);
        } catch (InterruptedException e){}

        return cidadeRepository.findByEstadoCodigo(codigoEstado);
    }

    @DeleteMapping("{codigo}")
    public @ResponseBody
    ResponseEntity<?> excluir(@PathVariable("codigo") Long codigo){
        cadastroCidadeService.excluir(codigo);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/{codigo}")
    public ModelAndView editar(@PathVariable("codigo") Cidade cidade){
        ModelAndView modelAndView = novo(cidade);
        modelAndView.addObject(cidade);
        return modelAndView;
    }

}
