package com.senac.stock.controller;

import com.senac.stock.model.Cliente;
import com.senac.stock.model.TipoPessoa;
import com.senac.stock.repository.ClienteRepository;
import com.senac.stock.repository.EstadoRepository;
import com.senac.stock.repository.filter.ClienteFilter;
import com.senac.stock.service.CadastroClienteService;
import com.senac.stock.service.exception.CpfOuCnpjClienteJaCadastradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/clientes")
public class ClientesController {

    @Autowired
    private EstadoRepository estadoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private CadastroClienteService cadastroClienteService;

    @GetMapping("/novo")
    public ModelAndView novo(Cliente cliente) {
        ModelAndView modelAndView = new ModelAndView("cliente/CadastroCliente");
        modelAndView.addObject("tiposPessoa", TipoPessoa.values());
        modelAndView.addObject("estados", estadoRepository.findAll());

        return modelAndView;
    }

    @PostMapping({"/novo", "{\\d+}"})
    public ModelAndView salvar(@Valid Cliente cliente, BindingResult result, RedirectAttributes redirectAttributes){
        if(result.hasErrors()){
            return novo(cliente);
        }

        try {
            cadastroClienteService.salvar(cliente);
        } catch (CpfOuCnpjClienteJaCadastradoException e){
            result.rejectValue("cpfOuCnpj", e.getMessage(), e.getMessage());
            return novo(cliente);
        }

        redirectAttributes.addFlashAttribute("mensagem", "Cliente salvo com sucesso!");

        return new ModelAndView("redirect:/clientes");
    }

    @GetMapping()
    public ModelAndView pesquisar(ClienteFilter clienteFilter, BindingResult result, Pageable pageable){
        ModelAndView modelAndView = new ModelAndView("cliente/PesquisaClientes");

        modelAndView.addObject("clientes", clienteRepository.filtrar(clienteFilter));
        return modelAndView;
    }

    @DeleteMapping("{codigo}")
    public @ResponseBody
    ResponseEntity<?> excluir(@PathVariable("codigo") Long codigo){
        cadastroClienteService.excluir(codigo);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/{codigo}")
    public ModelAndView editar(@PathVariable("codigo") Cliente cliente){
        ModelAndView modelAndView = novo(cliente);
        modelAndView.addObject(cliente);
        return modelAndView;
    }

    @GetMapping(consumes = { MediaType.APPLICATION_JSON_VALUE })
    public @ResponseBody List<Cliente> pesquisar(String nome){
        validarTamanhoNome(nome);
        return clienteRepository.findByNomeStartingWithIgnoreCase(nome);
    }

    private void validarTamanhoNome(String nome) {
        if(StringUtils.isEmpty(nome) || nome.length() < 3){
            throw new IllegalArgumentException();
        }
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Void> tratarIllegalArgumentException(IllegalArgumentException e){
        return ResponseEntity.badRequest().build();
    }
}
