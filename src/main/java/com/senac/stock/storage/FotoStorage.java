package com.senac.stock.storage;

import org.springframework.web.multipart.MultipartFile;

public interface FotoStorage {

    public String salvarTemporariamente(MultipartFile[] files);

    public byte[] recuperarFotoTemp(String nome);

    void salvar(String foto);

    byte[] recuperar(String foto);

    public void excluir(String foto);
}
