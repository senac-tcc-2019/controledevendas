package com.senac.stock.storage.local;

import com.senac.stock.storage.FotoStorage;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

public class FotoStorageLocal implements FotoStorage {

    private static final Logger logger = LoggerFactory.getLogger(FotoStorageLocal.class);
    private static final String THUMBNAIL_PREFIX = "thumbnail.";

    private Path local;
    private Path localTemporario;

    public FotoStorageLocal(){
        this.local = FileSystems.getDefault().getPath(System.getenv("USERPROFILE"), ".stockfotos");
        criarPastas();
    }

    @Override
    public String salvarTemporariamente(MultipartFile[] files) {
        String novoNome = null;
        if(files != null && files.length > 0){
            MultipartFile arquivo = files[0];
            novoNome = renomearFoto(arquivo.getOriginalFilename());
            try {
                arquivo.transferTo(new File(this.localTemporario.toAbsolutePath().toString() + FileSystems.getDefault().getSeparator() + novoNome));
            } catch (IOException e) {
                throw new RuntimeException("Erro ao salvar a foto na pasta temp", e);
            }
        }
        return novoNome;
    }

    @Override
    public byte[] recuperarFotoTemp(String nome) {
        try {
            return Files.readAllBytes(this.localTemporario.resolve(nome));
        }catch (IOException e){
            throw  new RuntimeException("Erro ao ler a foto temp", e);
        }

    }

    @Override
    public void salvar(String foto) {
        try {
            Files.move(this.localTemporario.resolve(foto), this.local.resolve(foto));
        } catch (IOException e) {
            throw new RuntimeException("Erro movendo a foto", e);
        }

        try {
            Thumbnails.of(this.local.resolve(foto).toString()).size(40, 68).toFiles(Rename.PREFIX_DOT_THUMBNAIL);
        } catch (IOException e){
            throw new RuntimeException("Erro gerando a thumb", e);
        }

    }

    @Override
    public byte[] recuperar(String nome) {
        try {
            return Files.readAllBytes(this.local.resolve(nome));
        }catch (IOException e){
            throw  new RuntimeException("Erro ao ler a foto", e);
        }
    }

    @Override
    public void excluir(String foto) {
        try {
            Files.deleteIfExists(this.local.resolve(foto));
            Files.deleteIfExists(this.local.resolve(THUMBNAIL_PREFIX + foto));
        }catch (IOException e){ }
    }

    private void criarPastas(){
        try {
            Files.createDirectories(this.local);
            this.localTemporario = FileSystems.getDefault().getPath(this.local.toString(), "temp");
            Files.createDirectories(this.localTemporario);

            if(logger.isDebugEnabled()){
                logger.debug("Pastas criadas para salvar a foto");
                logger.debug("Pasta default: "+ this.local.toAbsolutePath());
                logger.debug("Pasta temporária: "+ this.localTemporario.toAbsolutePath());
            }
        } catch (IOException e) {
            throw new RuntimeException("Erro ao criar pasta para salvar a foto", e);
        }
    }

    private String renomearFoto(String nomeOriginal){
        String novoNome = UUID.randomUUID().toString()+"_"+ nomeOriginal;

        return novoNome;
    }
}
