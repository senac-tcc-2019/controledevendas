package com.senac.stock.model;

import com.senac.stock.validation.SKU;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.*;


import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Getter
@Setter
public class Vinho {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codigo;

    @SKU
    @NotBlank(message = "SKU é obrigatório")
    private String sku;

    @NotBlank(message = "Nome é obrigatório")
    private String nome;

    @Size(min = 1, max = 50, message = "Descrição deve estar entre 1 e 50 caracteres")
    private String descricao;

    @NotNull(message = "Valor é obrigatório")
    @DecimalMin(value = "3.00", message = "Valor do vinho deve ser maior que 3,00")
    //@DecimalMax(value = "999", message = "Valor deve ser menor que 1000")
    private BigDecimal valor;

    @NotNull(message = "Teor Alcóolico é obrigatório")
    @DecimalMax(value = "99.0", message = "Teor alcóolico deve ser menor que 100")
    @Column(name = "teor_alcoolico")
    private BigDecimal teorAlcoolico;

    @DecimalMax(value = "99.0", message = "Comissão deve ser menor que 100")
    @NotNull(message = "Comissão é obrigatório")
    private BigDecimal comissao;

    @DecimalMax(value = "999", message = "Estoque deve ser menor que 1000")
    @Column(name = "quantidade_estoque")
    private Integer quantidadeEstoque;

    @NotNull(message = "Origem é obrigatório")
    @Enumerated(EnumType.STRING)
    private Origem origem;

    @NotNull(message = "Uva é obrigatório")
    @Enumerated(EnumType.STRING)
    private Uva uva;

    @NotNull(message = "Tipo é obrigatório")
    @ManyToOne
    @JoinColumn(name = "codigo_tipo")
    private Tipo tipo;

    private String foto;

    @Column(name = "estoque_minimo")
    private Integer estoqueMinimo;

    @Column(name = "content_type")
    private String contentType;


    @PrePersist
    @PreUpdate
    private void prePersist(){
        sku = sku.toUpperCase();
    }


    public String getFotoOuMock(){
        return !StringUtils.isEmpty(foto) ? foto : "mock-vinho.jpeg";
    }

    public boolean isNova(){
        return codigo == null;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vinho vinho = (Vinho) o;
        return codigo.equals(vinho.codigo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }
}
