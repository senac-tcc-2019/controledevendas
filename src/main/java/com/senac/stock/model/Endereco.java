package com.senac.stock.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Getter
@Setter
@Embeddable
public class Endereco {

    private String logradouro;

    private String numero;

    private String complemento;

    private String cep;

    @ManyToOne
    @JoinColumn(name = "codigo_cidade")
    private Cidade cidade;

    @Transient
    private Estado estado;

    public Estado getEstado() {
        if (this.cidade != null) {
            return this.cidade.getEstado();
        }

        return null;
    }

    public String getNomeCidadeSiglaEstado(){
        if(this.cidade != null){
            return this.cidade.getNome() + "/" + this.cidade.getEstado().getSigla();
        }

        return null;
    }
}
