package com.senac.stock.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
@Getter
@Setter
public class Estado {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codigo;

    @NotBlank(message = "Nome é obrigatório")
    private String nome;

    private String sigla;

    @OneToMany(mappedBy = "estado")
    private List<Cidade> cidades;

    public boolean isNova(){
        return codigo == null;
    }
}
