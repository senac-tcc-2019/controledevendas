package com.senac.stock.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.senac.stock.model.validation.ClienteGroupSequenceProvider;
import com.senac.stock.model.validation.group.CnpjGroup;
import com.senac.stock.model.validation.group.CpfGroup;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.br.CNPJ;
import org.hibernate.validator.constraints.br.CPF;
import org.hibernate.validator.group.GroupSequenceProvider;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Getter
@Setter
@GroupSequenceProvider(ClienteGroupSequenceProvider.class)
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codigo;

    @NotBlank(message = "Nome é obrigatório")
    private String nome;

    @NotNull(message = "Tipo pessoa é obrigatório")
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_pessoa")
    private TipoPessoa tipoPessoa;

    @NotNull(message = "CPF/CNPJ é obrigatório")
    @CPF(groups = CpfGroup.class)
    @CNPJ(groups = CnpjGroup.class)
    @Column(name = "cpf_cnpj")
    private String cpfOuCnpj;

    @NotNull(message = "Telefone é obrigatório")
    private String telefone;

    @Email(message = "Email inválido")
    private String email;

    @JsonIgnore
    @Embedded
    private Endereco endereco;

    public boolean isNova(){
        return codigo == null;
    }



    @PrePersist
    @PreUpdate
    private void prePersistPreUpdate(){
        this.cpfOuCnpj = TipoPessoa.removerFormatacao(this.cpfOuCnpj);
    }

    @PostLoad
    private void postLoad(){
        this.cpfOuCnpj = this.tipoPessoa.formatar(this.cpfOuCnpj);
    }

    public String getCpfOuCnpjSemFormatacao(){
        return TipoPessoa.removerFormatacao(this.cpfOuCnpj);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return Objects.equals(codigo, cliente.codigo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }
}
