package com.senac.stock.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
public class Tipo{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codigo;

    @NotBlank(message = "Nome é obrigatório")
    //@Pattern(regexp = "([a-zA-z])?", message = "Permitido somente letras")
    private String nome;

    @OneToMany(mappedBy = "tipo")
    private List<Vinho> vinhos;

    public boolean isNova(){
        return codigo == null;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tipo tipo = (Tipo) o;
        return codigo.equals(tipo.codigo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }
}
