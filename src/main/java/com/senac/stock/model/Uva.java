package com.senac.stock.model;

public enum Uva {

    MALBEC("Malbec"),
    CARMENERE("Carménère"),
    TANNAT("Tannat"),
    TEMPRANILLO("Tempranillo");

    private String descricao;

    Uva(String descricao){
        this.descricao = descricao;
    }

    public String getDescricao(){
        return descricao;
    }
}
