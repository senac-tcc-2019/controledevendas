package com.senac.stock.model.validation;

import com.senac.stock.model.Cliente;
import org.hibernate.validator.spi.group.DefaultGroupSequenceProvider;

import java.util.ArrayList;
import java.util.List;

public class ClienteGroupSequenceProvider implements DefaultGroupSequenceProvider<Cliente> {


    @Override
    public List<Class<?>> getValidationGroups(Cliente cliente) {
        List<Class<?>> grupos = new ArrayList<>();

        grupos.add(Cliente.class);

        if(cliente != null && cliente.getTipoPessoa() != null){
            grupos.add(cliente.getTipoPessoa().getGrupo());
        }

        return grupos;
    }
}
