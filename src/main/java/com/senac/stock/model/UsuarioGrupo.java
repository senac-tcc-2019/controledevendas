package com.senac.stock.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "usuario_grupo")
@Getter
@Setter
@EqualsAndHashCode
public class UsuarioGrupo {

    @EmbeddedId
    private UsuarioGrupoId id;
}
