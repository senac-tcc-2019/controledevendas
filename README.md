# Stock Control
## Sistema web responsivo gerenciador de estoque.

### Artigo
* https://www.overleaf.com/read/dtdzxjgbxnck

### Acesso Heroku
 * https://stock-control-senac.herokuapp.com
#### Administrador
 * Usuário: admin@stock.com
 * Senha: 123
#### Vendedor
 * Usuário: vinicius@stock.com
 * Senha: 123


### Como executar a aplicação
 * Clonar o repositório
 * Entrar na pasta do repositório
 * Cria um banco de dados mysql com o nome de stock
 * Se for necessário mudar as credencias do banco de dados, ir no arquivo pom.xml que fica na raiz do projeto
 * Agora é só rodar o projeto que ele irá baixar todas as dependências necessárias


### Rotas
 * localhost:8080/vinhos
 * localhost:8080/vinhos/novo
 * localhost:8080/tipos
 * localhost:8080/tipos/novo
 * localhost:8080/estados
 * localhost:8080/estados/novo
 * localhost:8080/cidades
 * localhost:8080/cidades/novo
 * localhost:8080/clientes
 * localhost:8080/clientes/novo
 * localhost:8080/usuarios/novo


